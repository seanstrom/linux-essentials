* Lock Screen
* Wifi
* Touchpad
* Audio
* Media keys
* Video
* Colemak
* Caps Lock mapped to Control and Escape
* Laptop power savings
* Lock screen on lid close
* Screen stays awake when watching videos

# Lock Screen
physlock

# Wifi

# Touchpad
50-synaptics.conf

# Audio
alsa

# Video
flash

# Keyboard

## Colemak
20-keyboard.conf

## Caps as Ctrl and ESC
xcape

## Console Keyboard
loadkeys

