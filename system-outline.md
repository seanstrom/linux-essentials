
# System Outline
This document is meant to serve as a general overview of what software and hardware I run.  
I intend to keep track of many different pieces of my system and refer to different documents that describe those  
pieces more in depth. I also intend to keep track of any NON-FREE Software I use, and the alternatives I am considering.  

## Hardware
At the moment for my personal laptop I use a Dell Precision M3800 - Developer Edition.  
Which includes:

* 256GB SSD
* 16GB 1600MHz DDR3L RAM
* Intel Dual Band Wireless-AC 7260 @ 5GHz + Bluetooth 4.0
* 91 WHr 6-cell battery
* Intel Core i7-4712HQ
  * Quad Core 2.30GHz, 3.3GHz Turbo
  * 6MB
  * 37W
  * HD Graphics 4600, Thunderbolt Ready
* 15.6-inch FHD (1920-1080) touch display

Amongst other details that can be seen [here](http://www.dell.com/us/business/p/precision-m3800-workstation/pd?oc=pm38001516c1f02&model_id=precision-m3800-workstation)

## Software

### Operating System

Current:  
At the moment I use Manjaro Linux, which is a distribution of Arch Linux.  
It's really easy to get non-free software in Arch, but I try to use free software.  
I may very well be using some proprietary drivers, but I haven't investigated yet.  

Alternatives:  
If I still want to use Arch, I can move to [Parabola GNU](https://www.parabola.nu/).  
Or I can refer to any distro listed [here](http://www.gnu.org/distros/free-distros.en.html).  
All together I can still use Manjaro or Debian for now, but I intend to move to a free OS in the future.


### Web Browser

Current:  
This has got to be one of my most used applications in my system, I use this one all day.  
I typically find myself using a combination of Chromium, Chrome, and Firefox. The only offender being actual
Chrome, for not being free software. I try to stick to Chromium, but I've occasionally open up Chrome for it's rather stable
Flash integration.

Alternatives:  
I can continue to use Chromium and Firefox, but with Chromium I don't have access to the LibreJS plugin.  
I can use this plugin with Firefox, but I feel Firefox is a bit bloated compared to Chromium, at least aesthetically.  
I could also try derivatives of Firefox like: IceWeasel, IceCat, and so on.

### Web Browser Plugins

Current:  
I use many browser plugins in Chromium, and I only use one in Firefox.
Here's the list:

Chromium:
* vimium w/ smooth scrolling
* getpocket extensions
* instapaper
* adblock plus
* ghostery
* browser stack local
* feedly
* json viewer
* workflowy

Firefox:
* vimfx

Alternatives:  
I still need to investigate how many of my browser plugins are free software.


### Terminal

Currently:  
I use urxvt, I often used urxvtd and urxvtc  
I also use some perl extensions and even went and installed the patched version of urxvt because of font issues.

Alternatives:  
I've heard of st, or Simple Terminal, I would use that, but I haven't figured out how to configure it hehe.

### Editor

Currently:  
I use mostly VIM, I occasionally dabble in Emacs.

Alternatives:  
I wish to use Emacs, maybe even the GUI version. I've just had trouble converting my setup over to
Emacs. Part of the issue is my existing Tmux setup conflicts with some Emacs Binding, specifically C-Space.
If I didn't have that issue I would move to Emacs, and learn from there. I've also tried Spacemacs, which is pretty good, but
I felt like I didn't know much about what I was doing. I want to learn emacs.

### Video Player

Currently:  
I usually watch videos online, so now desktop client atm.

Alternatives:  
VLC

### Music Player

Currently:  
I use Spotify a ton. I've been using the Desktop Client, I wish there was a command line client.

Alternatives:  
Pianobar with Pandora is one option. I need to find a music streaming client that is free software, and be able to
pair that with music streaming services.

### Torrent Client

Currently:  
Well I never Torrent so...
If I was to hypothetically torrent, I may use Transmission.

Alternatives:  
I've heard of several Torrent clients but none that strike me as a strong alternative.
More research required.

### Email

Current:  
All my email at the moment goes through Gmail. My work email, my personal email, just all of it.  
I personally don't like the idea of using Gmail anymore. I'm getting more and more concerned with privacy and
making sure my data isn't used in ways I don't want.

Alternatives:  
I recently stumbled across a service named [Posteo](posteo.de/en). They seem to a cool service that will handle my email
needs, while also respecting my privacy and what not. They also have worked closely with FSF's FreeJS campaign in order to make
the client Javascript on their sit totally free. Pretty cool.

I'll be looking into other alternatives as well.

### Email Client

Current:  
I've mainly used the Gmail web client for all of my Email needs.  
I occasionally use a Desktop client like Mailbox, Gmail App, Thunderbird, or Geary.  
But I've been interested in trying a commandline email client, something like Pine or Mutt.

### Calendar

Current:  
I only really use a calendar for work. That's typically Google Calendar.

Alternatives:  
I'm not sure what could integrate with my work calendar, but if I found a service that could do that
I'll consider switching.

### Documents

Current:  
I've used apps like Google Docs, Keynote, and Word in the past. I don't currently use those apps but I need to
find replacements.

Alternatives:  
Google Docs and Word tools can be replaced by Libre Office and such.
Keynote can also be replaced by Libre Office but I may want a better alternative.

### Graphics Editing

Current:  
In the past I've used tools like PhotoShop, Sketch, and Illustrator. Nowadays I don't do much graphic edting,
but if I had to I need to find alternatives

Alternatives:  
At the moment I can only think of Gimp or GimpShop, but I'm not sure if they handle vector based images.
if not I need to find a replacement for that.

### Drivers

Current:  
The only drivers that may be proprietary are the WiFi and GPU drivers

Alternatives:  
there are several alternatives, i just need to find there names.

### DE, WM, ETC.

Current:  
I've been recently using Awesome WM as WM/DE

Alternatives:  
I've only really considered moving to Xmonad, but I may try BSPWM.

### File Manager

Currently:  
SpaceFM is my current file manager. It seems pretty good, no complaints

Alternatives:  
Perhaps Nautilus.

### Kernel Manager

Currently:  
I have been using the Manjaro Kernel Manager

Alternatives:  
Not sure if one exists, but Im sure I could create a suite of bash scripts

### Display Manager

Currently:  
Arandr and Xrandr

Alternatives:
Not sure if any are needed but I'll keep an eye out.

### Chat Clients

### Other

### Version Control Software

Currently:  
I use Git, pretty good so far ;)

Alternatives:  
The only alternative I've considered it Mercurial.
More research required.

## Services

### Bookmarking

Currently:  
Pocket
Instapaper

### Note Taking

Currently:  
Evernote

### Email -- See above

### Knowledge Centers

Currently:  
Railscasts
Upcase

there's more, needs more research

### Social Media / News

Currently:  
Twitter
Reddit
HackerNews
Feedly
EchoJS

Alternatives:  
Look into MediaGoblin

### Chat Services

Currently:  
Hipchat
Slack

### Music Services

Currently:  
Spotify

### Code Hosting and Collaboration

Currently:  
Github

Alternatives:  
GitLab
